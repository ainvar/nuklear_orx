-- This premake script should be used with orx-customized version of premake4.
-- Its Hg repository can be found at https://bitbucket.org/orx/premake-stable.
-- A copy, including binaries, can also be found in the extern/premake folder.

--
-- Globals
--

function initconfigurations ()
    return
    {
        "Debug",
        "Release",
    }
end

function initplatforms ()
    if os.is ("windows") then
        if string.lower(_ACTION) == "vs2013"
        or string.lower(_ACTION) == "vs2015"
        or string.lower(_ACTION) == "vs2017" then
            return
            {
                "x64",
                "x32"
            }
        else
            return
            {
                "Native"
            }
        end
    elseif os.is ("linux") then
        if os.is64bit () then
            return
            {
                "x64",
                "x32"
            }
        else
            return
            {
                "x32",
                "x64"
            }
        end
    elseif os.is ("macosx") then
        if string.find(string.lower(_ACTION), "xcode") then
            return
            {
                "Universal"
            }
        else
            return
            {
                "x32", "x64"
            }
        end
    end
end

function defaultaction (name, action)
   if os.is (name) then
      _ACTION = _ACTION or action
   end
end

defaultaction ("windows", "vs2017")
defaultaction ("linux", "gmake")
defaultaction ("macosx", "gmake")

newoption
{
    trigger = "to",
    value   = "path",
    description = "Set the output location for the generated files"
}

if os.is ("macosx") then
    osname = "mac"
else
    osname = os.get()
end

destination = _OPTIONS["to"] or "./" .. osname .. "/" .. _ACTION
copybase = path.rebase ("..", os.getcwd (), os.getcwd () .. "/" .. destination)


--
-- Solution: nuklear_orx
--

solution "nuklear_orx"

    language ("C++")

    location (destination)

    configurations
    {
        initconfigurations ()
    }

    platforms
    {
        initplatforms ()
    }

    includedirs
    {
        "../include",
        "../nuklear",
        os.getenv('ORX').."/include",
    }

    excludes {}

    flags
    {
        "NoPCH",
        "NoManifest",
        "FloatFast",
        "NoNativeWChar",
        "NoExceptions",
        "NoIncrementalLink",
        "NoEditAndContinue",
        "NoMinimalRebuild",
        "Symbols",
        "StaticRuntime"
    }

    configuration {"not vs2013", "not vs2015", "not vs2017"}
        flags {"EnableSSE2"}

    configuration {"not x64"}
        flags {"EnableSSE2"}

    configuration {"not windows"}
        flags {"Unicode"}

    configuration {"*Debug*"}
        targetsuffix ("d")
        defines {"__orxDEBUG__"}

    configuration {"*Release*"}
        flags {"Optimize", "NoRTTI"}


-- Linux

    configuration {"linux"}
        buildoptions { "-Wno-unused-function", "-Wno-unused-but-set-variable" }


-- Mac OS X

    configuration {"macosx"}
        buildoptions { "-x c++", "-gdwarf-2", "-Wno-write-strings", "-fvisibility-inlines-hidden" }
        linkoptions { "-dead_strip" }

    configuration {"macosx", "x32"}
        buildoptions { "-mfix-and-continue" }


-- Windows

    configuration {"windows", "vs*"}
        buildoptions { "/MP" }

--
-- Project: imgui_orx_test
--

project "nuklear_orx_test"
    files {"../test/main.cpp"}

    targetdir ("../bin")
    kind ("ConsoleApp")

    configuration {"Debug"}
        links { os.getenv('ORX').."/lib/dynamic/orxd", }
    configuration {"not *Debug*"}
        links { os.getenv('ORX').."/lib/dynamic/orx" }


-- Linux

    configuration {"linux"}
        linkoptions {"-Wl,-rpath ./", "-Wl,--export-dynamic"}
        postbuildcommands 
        {
            "cp -f " .. copybase .. "/test/*.ini " .. copybase .. "/bin",
            "cp -f " .. os.getenv('ORX').."/lib/dynamic/*.so " .. copybase .. "/bin",
            "cp -f " .. copybase .. "/nuklear/extra_font/*.* " .. copybase .. "/bin",
        }

    -- This prevents an optimization bug from happening with some versions of gcc on linux
    configuration {"linux", "not *Debug*"}
        buildoptions {"-fschedule-insns"}
        postbuildcommands 
        {
            "cp -f " .. copybase .. "/test/*.ini " .. copybase .. "/bin",
            "cp -f " .. os.getenv('ORX').."/lib/dynamic/*.so " .. copybase .. "/bin",
            "cp -f " .. copybase .. "/nuklear/extra_font/*.* " .. copybase .. "/bin",
        }


-- Mac OS X

    configuration {"macosx", "not codelite", "not codeblocks"}
        links
        {
            "Foundation.framework",
            "IOKit.framework",
            "AppKit.framework"
        }
        postbuildcommands 
        {
            "cp -f " .. copybase .. "/test/*.ini " .. copybase .. "/bin",
            "cp -f " .. os.getenv('ORX').."/lib/dynamic/*.so " .. copybase .. "/bin",
            "cp -f " .. copybase .. "/nuklear/extra_font/*.* " .. copybase .. "/bin",
        }

    configuration {"macosx", "codelite or codeblocks"}
        linkoptions
        {
            "-framework Foundation",
            "-framework IOKit",
            "-framework AppKit"
        }
    configuration {"macosx"}
        links
        {
            "pthread"
        }
        postbuildcommands 
        {
            "cp -f " .. copybase .. "/test/*.ini " .. copybase .. "/bin",
            "cp -f " .. os.getenv('ORX').."/lib/dynamic/*.so " .. copybase .. "/bin",
            "cp -f " .. copybase .. "/nuklear/extra_font/*.* " .. copybase .. "/bin",
        }


-- Windows

    configuration {"windows"}
        defines {"_CRT_SECURE_NO_WARNINGS"}
        linkoptions{"/DEBUG:FULL"}

        postbuildcommands 
        {
            "cmd /c copy /Y " .. path.translate(copybase, "\\") .. "\\test\\*.ini " .. path.translate(copybase, "\\") .. "\\bin",
            "cmd /c copy /Y " .. os.getenv('ORX').."\\lib\\dynamic\\orx*.dll " .. path.translate(copybase, "\\") .. "\\bin",
            "cmd /c copy /Y " .. path.translate(copybase, "\\") .."\\nuklear\\extra_font\\*.* " .. path.translate(copybase, "\\") .. "\\bin"
        }
        links
        {
            "winmm"
        }
        postbuildcommands 
        {
            "cmd /c copy /Y " .. path.translate(copybase, "\\") .. "\\test\\*.ini " .. path.translate(copybase, "\\") .. "\\bin",
            "cmd /c copy /Y " .. os.getenv('ORX').."\\lib\\dynamic\\orx*.dll " .. path.translate(copybase, "\\") .. "\\bin",
            "cmd /c copy /Y " .. path.translate(copybase, "\\") .."\\nuklear\\extra_font\\*.* " .. path.translate(copybase, "\\") .. "\\bin"
        }
