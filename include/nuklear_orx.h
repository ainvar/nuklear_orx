/*
* Nuklear integration with Orx
*/

/*
* ==============================================================
*
*                              API
*
* ===============================================================
*/


#ifndef __NUKLEAR_ORX_HEADER__
#define __NUKLEAR_ORX_HEADER__

#define NUKLEAR_ORX_VERSION_MAJOR       0
#define NUKLEAR_ORX_VERSION_MINOR       1
#define NUKLEAR_ORX_VERSION_PATCH       0


#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_IMPLEMENTATION
#define NK_IMPLEMENTATION

#include "../nuklear/nuklear.h"

#ifdef __cplusplus
extern "C"
    {
#endif 

#include <orx.h>



    NK_API struct nk_context*               nk_orx_init(void);
    NK_API void                             nk_orx_font_stash_begin(struct nk_font_atlas ** atlas);
    NK_API void                             nk_orx_font_stash_end(void);
    NK_API void                             nk_orx_new_frame(void);
    NK_API void                             nk_orx_render(enum nk_anti_aliasing eAntiAliasing);
    NK_API void                             nk_orx_shutdown(void);


    static orxBITMAP * nk_orx_upload_font_bitmap(const void * image, int width, int height);
    static void nk_orx_clear_font_bitmap(orxBITMAP * _pstBitmap);
    static void orxFASTCALL nk_orx_mouse_wheel_update(const orxCLOCK_INFO *_pstClockInfo, void *_pContext);



    //////////////////////////////////////////////////////////////////////////
    //                              DATA TYPES
    //////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////
    //! Static module data
    typedef struct _nk_orx_module_data
        {
        //! Current font texture
        orxBITMAP *                 pstFontTexture;
        //! clock used to check wheel delta
        orxCLOCK *                  pstMainClock;
        //! Draw commands buffer
        struct nk_buffer            stCommands;
        //! Null texture
        struct nk_draw_null_texture stNullTexture;
        //! Nuklear context
        struct nk_context           stContext;
        //! Nuklear font atlas
        struct nk_font_atlas        stFontAtlas;
        //! Last button pressed
        unsigned long               ulLastButtonClick;
        //! Mouse wheel info
        struct nk_vec2              stWheel;

        } nk_orx_module_data;


    //////////////////////////////////////////////////////////////////////////
    //                              INTERNAL FUNCTIONS
    //////////////////////////////////////////////////////////////////////////

    //! Called once to initialize the font texture
    orxBITMAP * nk_orx_upload_font_bitmap(const void * image, int width, int height);
    //! Called once to initialize the font texture
    void nk_orx_clear_font_bitmap(orxBITMAP * _pstBitmap);
    //! Updates mouse wheel state, called using gModuleData.m_pstMainClock
    void orxFASTCALL nk_orx_mouse_wheel_update(const orxCLOCK_INFO *_pstClockInfo, void *_pContext);

    //////////////////////////////////////////////////////////////////////////
    //                              GLOBAL DATA
    ///////////////////////////

    //! Global module data instance
    static nk_orx_module_data gModuleData;


  
    //////////////////////////////////////////////////////////////////////////
    NK_API struct nk_context * nk_orx_init(void)
        {
        /* clear the module data */
        memset(&gModuleData, 0, sizeof(nk_orx_module_data));

        /* initialize commands buffer */
        nk_buffer_init_default(&gModuleData.stCommands);

        /* initialize default context */
        nk_init_default(&gModuleData.stContext, 0);

        /* install orx clock callback to update mouse wheel status */
        gModuleData.pstMainClock = orxClock_FindFirst(orx2F(-1.0f), orxCLOCK_TYPE_CORE);
        if (gModuleData.pstMainClock != orxNULL)
            orxClock_Register(gModuleData.pstMainClock, nk_orx_mouse_wheel_update, orxNULL, orxMODULE_ID_INPUT, orxCLOCK_PRIORITY_HIGH);

        return &gModuleData.stContext;
        }

    //////////////////////////////////////////////////////////////////////////
    NK_API void nk_orx_font_stash_begin(struct nk_font_atlas ** atlas)
        {
        nk_font_atlas_init_default(&gModuleData.stFontAtlas);
        nk_font_atlas_begin(&gModuleData.stFontAtlas);
        *atlas = &gModuleData.stFontAtlas;
        }

    //////////////////////////////////////////////////////////////////////////
    NK_API void nk_orx_font_stash_end(void)
        {
        const void * image;
        int imageWidth = 0;
        int imageHeight = 0;

        /* create font atlas bake */
        image = nk_font_atlas_bake(&gModuleData.stFontAtlas, &imageWidth, &imageHeight, NK_FONT_ATLAS_RGBA32);

        /* upload the bitmap to the device */
        gModuleData.pstFontTexture = nk_orx_upload_font_bitmap(image, imageWidth, imageHeight);

        /* end of font config */
        nk_font_atlas_end(&gModuleData.stFontAtlas, nk_handle_ptr((void *)gModuleData.pstFontTexture), &gModuleData.stNullTexture);

        /* set the font as default font */
        if (gModuleData.stFontAtlas.default_font)
            nk_style_set_font(&gModuleData.stContext, &gModuleData.stFontAtlas.default_font->handle);
        }

    //////////////////////////////////////////////////////////////////////////
    NK_API void nk_orx_new_frame(void)
        {
        // Setup display size (every frame to accommodate for window resizing)
        orxFLOAT display_w, display_h;

        /* get screen size */
        orxDisplay_GetScreenSize(&display_w, &display_h);

        nk_input_begin(&gModuleData.stContext);

        const orxSTRING zString = orxKeyboard_ReadString();
        if (zString)
            {
            orxU32 uiStringLen = orxString_GetLength(zString);
            if (uiStringLen)
                {
                for (orxU32 i = 0; i < uiStringLen; ++i)
                    nk_input_char(&gModuleData.stContext, zString[i]);
                }
            }

        nk_input_key(&gModuleData.stContext, NK_KEY_DEL, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_DELETE));
        nk_input_key(&gModuleData.stContext, NK_KEY_ENTER, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_ENTER));
        nk_input_key(&gModuleData.stContext, NK_KEY_TAB, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_TAB));
        nk_input_key(&gModuleData.stContext, NK_KEY_BACKSPACE, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_BACKSPACE));
        nk_input_key(&gModuleData.stContext, NK_KEY_UP, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_UP));
        nk_input_key(&gModuleData.stContext, NK_KEY_DOWN, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_DOWN));
        nk_input_key(&gModuleData.stContext, NK_KEY_TEXT_START, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_HOME));
        nk_input_key(&gModuleData.stContext, NK_KEY_TEXT_END, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_END));
        nk_input_key(&gModuleData.stContext, NK_KEY_SCROLL_START, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_HOME));
        nk_input_key(&gModuleData.stContext, NK_KEY_SCROLL_END, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_END));
        nk_input_key(&gModuleData.stContext, NK_KEY_SCROLL_DOWN, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_PAGE_DOWN));
        nk_input_key(&gModuleData.stContext, NK_KEY_SCROLL_UP, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_PAGE_UP));
        nk_input_key(&gModuleData.stContext, NK_KEY_SHIFT, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_LSHIFT) || orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_RSHIFT));

        if (orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_LCTRL) || orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_RCTRL))
            {
            nk_input_key(&gModuleData.stContext, NK_KEY_COPY, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_C));
            nk_input_key(&gModuleData.stContext, NK_KEY_PASTE, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_V));
            nk_input_key(&gModuleData.stContext, NK_KEY_CUT, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_X));
            nk_input_key(&gModuleData.stContext, NK_KEY_TEXT_UNDO, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_Z));
            nk_input_key(&gModuleData.stContext, NK_KEY_TEXT_REDO, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_R));
            nk_input_key(&gModuleData.stContext, NK_KEY_TEXT_WORD_LEFT, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_LEFT));
            nk_input_key(&gModuleData.stContext, NK_KEY_TEXT_WORD_RIGHT, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_RIGHT));
            nk_input_key(&gModuleData.stContext, NK_KEY_TEXT_LINE_START, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_B));
            nk_input_key(&gModuleData.stContext, NK_KEY_TEXT_LINE_END, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_E));
            }
        else
            {
            nk_input_key(&gModuleData.stContext, NK_KEY_LEFT, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_LEFT));
            nk_input_key(&gModuleData.stContext, NK_KEY_RIGHT, orxKeyboard_IsKeyPressed(orxKEYBOARD_KEY_RIGHT));
            nk_input_key(&gModuleData.stContext, NK_KEY_COPY, 0);
            nk_input_key(&gModuleData.stContext, NK_KEY_PASTE, 0);
            nk_input_key(&gModuleData.stContext, NK_KEY_CUT, 0);
            nk_input_key(&gModuleData.stContext, NK_KEY_SHIFT, 0);
            }

        orxVECTOR stMousePos = orxVECTOR_0;
        orxMouse_GetPosition(&stMousePos);
        nk_input_motion(&gModuleData.stContext, (int)stMousePos.fX, (int)stMousePos.fY);

        nk_input_button(&gModuleData.stContext, NK_BUTTON_LEFT, (int)stMousePos.fX, (int)stMousePos.fY, orxMouse_IsButtonPressed(orxMOUSE_BUTTON_LEFT));
        nk_input_button(&gModuleData.stContext, NK_BUTTON_MIDDLE, (int)stMousePos.fX, (int)stMousePos.fY, orxMouse_IsButtonPressed(orxMOUSE_BUTTON_MIDDLE));
        nk_input_button(&gModuleData.stContext, NK_BUTTON_RIGHT, (int)stMousePos.fX, (int)stMousePos.fY, orxMouse_IsButtonPressed(orxMOUSE_BUTTON_RIGHT));

        nk_input_scroll(&gModuleData.stContext, gModuleData.stWheel);

        nk_input_end(&gModuleData.stContext);

        gModuleData.stWheel = nk_vec2(0, 0);
        }

    //////////////////////////////////////////////////////////////////////////
    void nk_orx_render(enum nk_anti_aliasing eAntiAliasing)
        {
        // Avoid rendering when minimized, scale coordinates for retina displays (screen coordinates != framebuffer coordinates)
        struct nk_vec2 fb_scale = { 1, 1 };

        /* get screen size */
        orxFLOAT display_w, display_h;
        orxDisplay_GetScreenSize(&display_w, &display_h);

        // Gets screen bitmap
        orxBITMAP * pstScreen = orxDisplay_GetScreenBitmap();
        // Restores screen as destination bitmap
        orxDisplay_SetDestinationBitmaps(&pstScreen, 1);
        // Restores screen bitmap clipping
        orxDisplay_SetBitmapClipping(orxDisplay_GetScreenBitmap(), 0, 0, orxF2U(display_w), orxF2U(display_h));

        orxU32 vs = sizeof(orxDISPLAY_VERTEX);
        size_t vp = offsetof(orxDISPLAY_VERTEX, fX);
        size_t vt = offsetof(orxDISPLAY_VERTEX, fU);
        size_t vc = offsetof(orxDISPLAY_VERTEX, stRGBA);

        /* convert from command queue into draw list and draw to screen */

        /* fill convert configuration */
        struct nk_convert_config config;
        static const struct nk_draw_vertex_layout_element vertex_layout[] = {
            { NK_VERTEX_POSITION, NK_FORMAT_FLOAT, NK_OFFSETOF(orxDISPLAY_VERTEX, fX) },
            { NK_VERTEX_TEXCOORD, NK_FORMAT_FLOAT, NK_OFFSETOF(orxDISPLAY_VERTEX, fU) },
            { NK_VERTEX_COLOR, NK_FORMAT_R8G8B8A8, NK_OFFSETOF(orxDISPLAY_VERTEX, stRGBA) },
            { NK_VERTEX_LAYOUT_END }
            };

        NK_MEMSET(&config, 0, sizeof(config));
        config.vertex_layout = vertex_layout;
        config.vertex_size = sizeof(orxDISPLAY_VERTEX);
        config.vertex_alignment = NK_ALIGNOF(orxDISPLAY_VERTEX);
        config.null = gModuleData.stNullTexture;
        config.circle_segment_count = 22;
        config.curve_segment_count = 22;
        config.arc_segment_count = 22;
        config.global_alpha = 1.0f;
        config.shape_AA = eAntiAliasing;
        config.line_AA = eAntiAliasing;

        struct nk_buffer vbuf, ebuf;
        /* convert shapes into vertexes */
        nk_buffer_init_default(&vbuf);
        nk_buffer_init_default(&ebuf);
        nk_convert(&gModuleData.stContext, &gModuleData.stCommands, &vbuf, &ebuf, &config);

        /* setup vertex buffer pointer */
        const void *vertices = nk_buffer_memory_const(&vbuf);
        const void *elements = nk_buffer_memory_const(&ebuf);
        
        const struct nk_draw_command *cmd;
        /* iterate over and execute each draw command */
        const nk_draw_index * offset = (const nk_draw_index*)nk_buffer_memory_const(&ebuf);
        nk_draw_foreach(cmd, &gModuleData.stContext, &gModuleData.stCommands)
            {
            if (!cmd->elem_count)
                continue;

            orxDISPLAY_MESH stMesh = {};
            stMesh.ePrimitive = orxDISPLAY_PRIMITIVE_TRIANGLES;
            stMesh.astVertexList = (orxDISPLAY_VERTEX *)((nk_byte*)vertices + vp);
            stMesh.u32VertexNumber = gModuleData.stContext.draw_list.vertex_count;
            stMesh.au16IndexList = (orxU16 *)offset;
            stMesh.u32IndexNumber = cmd->elem_count;
            orxDisplay_SetBitmapClipping(orxNULL, (orxU32)cmd->clip_rect.x, (orxU32)cmd->clip_rect.y, (orxU32)(cmd->clip_rect.x + cmd->clip_rect.w), (orxU32)(cmd->clip_rect.y + cmd->clip_rect.h));
            orxDisplay_DrawMesh(&stMesh, (orxBITMAP *)cmd->texture.ptr, orxDISPLAY_SMOOTHING_OFF, orxDISPLAY_BLEND_MODE_ALPHA);

            offset += cmd->elem_count;
            }

        nk_clear(&gModuleData.stContext);
        nk_buffer_free(&vbuf);
        nk_buffer_free(&ebuf);
        }

    //////////////////////////////////////////////////////////////////////////
    NK_API void nk_orx_shutdown(void)
        {
        /* uninstall callback for mouse wheel status check */
        if (gModuleData.pstMainClock != orxNULL)
            orxClock_Unregister(gModuleData.pstMainClock, nk_orx_mouse_wheel_update);

        /* clear font atlas */
        nk_font_atlas_clear(&gModuleData.stFontAtlas);

        /* free nuklear context */
        nk_free(&gModuleData.stContext);

        /* delete texture used for fonts */
        nk_orx_clear_font_bitmap(gModuleData.pstFontTexture);

        /* free the commands buffer */
        nk_buffer_free(&gModuleData.stCommands);

        /* clear the module data */
        memset(&gModuleData, 0, sizeof(nk_orx_module_data));
        }

    //////////////////////////////////////////////////////////////////////////
    orxBITMAP * nk_orx_upload_font_bitmap(const void * image, int width, int height)
        {
        orxTEXTURE *pstTexture;
        orxBITMAP  *pstBitmap;

        // Creates background texture
        pstTexture = orxTexture_Create();
        pstBitmap = orxDisplay_CreateBitmap(width, height);
        orxDisplay_SetBitmapData(pstBitmap, (orxU8 *)image, width * height * sizeof(orxRGBA));
        orxTexture_LinkBitmap(pstTexture, pstBitmap, "Texture", orxTRUE);

        return pstBitmap;
        }


    //////////////////////////////////////////////////////////////////////////
    void nk_orx_clear_font_bitmap(orxBITMAP * _pstBitmap)
        {
        orxDisplay_DeleteBitmap(_pstBitmap);
        }

    //////////////////////////////////////////////////////////////////////////
    void orxFASTCALL nk_orx_mouse_wheel_update(const orxCLOCK_INFO *_pstClockInfo, void *_pContext)
        {
        orxFLOAT wheel_delta = orxMouse_GetWheelDelta();
        if (wheel_delta != orxFLOAT_0)
            gModuleData.stWheel = nk_vec2(0, wheel_delta);
        }





#ifdef __cplusplus
    }
#endif

#endif // __NUKLEAR_ORX_HEADER__