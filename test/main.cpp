// Example of using Nuklear with Orx Game Engine

#include <stdio.h>

//#define RUN_ORX_DEMO
   
//#define INCLUDE_ALL
//#define INCLUDE_STYLE
//#define INCLUDE_CALCULATOR
#define INCLUDE_OVERVIEW
//#define INCLUDE_NODE_EDITOR

#ifdef INCLUDE_ALL
#define INCLUDE_STYLE
#define INCLUDE_CALCULATOR
#define INCLUDE_OVERVIEW
#define INCLUDE_NODE_EDITOR
#endif

#include <nuklear_orx.h>

extern "C" 
    {
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <math.h>
#include <limits.h>
#include <time.h>
        
#ifdef INCLUDE_STYLE
#include "demo/style.c"
#endif
#ifdef INCLUDE_CALCULATOR
#include "demo/calculator.c"
#endif
#ifdef INCLUDE_OVERVIEW
#include "demo/overview.c"
#endif
#ifdef INCLUDE_NODE_EDITOR
#include "demo/node_editor.c"
#endif
    }

#ifdef UNUSED
#elif defined(__GNUC__)
# define UNUSED(x) UNUSED_ ## x __attribute__((unused))
#elif defined(__LCLINT__)
# define UNUSED(x) /*@unused@*/ x
#elif defined(__cplusplus)
# define UNUSED(x)
#else
# define UNUSED(x) x
#endif


/// GLOBAL VARIABLES

struct nk_context * m_pNkContext = orxNULL;
orxVIEWPORT * gpstMainViewport = orxNULL;
orxCAMERA * gpstMainCamera = orxNULL;

//////////////////////////////////////////////////////////////////////////
void NuklearOrx_ResizeViewport()
    {
    orxFLOAT scr_w, scr_h;
    orxDisplay_GetScreenSize(&scr_w, &scr_h);

    orxFLOAT vwp_w, vwp_h;
    orxViewport_GetSize(gpstMainViewport, &vwp_w, &vwp_h);

    orxAABOX frustum;
    orxCamera_GetFrustum(gpstMainCamera, &frustum);

    orxVECTOR cam_pos;
    orxCamera_GetPosition(gpstMainCamera, &cam_pos);
    orxCamera_SetFrustum(gpstMainCamera, vwp_w, vwp_h, frustum.vTL.fZ, frustum.vBR.fZ);
    orxCamera_SetPosition(gpstMainCamera, &cam_pos);

    orxDEBUG_PRINT(orxDEBUG_LEVEL_LOG, "Viewport Size : %f, %f", vwp_w, vwp_h);
    }


//////////////////////////////////////////////////////////////////////////
orxSTATUS orxFASTCALL NuklearOrx_EventHandler(const orxEVENT *_pstEvent) 
    {
    switch (_pstEvent->eType)
        {
        case orxEVENT_TYPE_RENDER:
            {
            /* we render Nuklear stuff on the top so we call it at the end of Orx frame rendering cycle */
            if (_pstEvent->eID == orxRENDER_EVENT_STOP)
                nk_orx_render(NK_ANTI_ALIASING_ON);
            }
            break;

        case orxEVENT_TYPE_DISPLAY:
            NuklearOrx_ResizeViewport();
            break;

        default:
            break;
        }
 
    return orxSTATUS_SUCCESS;
    }

//////////////////////////////////////////////////////////////////////////
orxSTATUS orxFASTCALL NuklearOrx_Init()
    {
    // Setup Nuklear binding
    m_pNkContext = nk_orx_init();

    struct nk_context * ctx = (struct nk_context *)m_pNkContext;

    struct nk_font_atlas *atlas;
    nk_orx_font_stash_begin(&atlas);

    struct nk_font * font_to_use = orxNULL;

    // font_to_use = nk_font_atlas_add_from_file(atlas, "./OpenSans.ttf", 14, 0);
    // font_to_use = nk_font_atlas_add_from_file(atlas, "./DroidSans.ttf", 14, 0);
    // font_to_use = nk_font_atlas_add_from_file(atlas, "./Roboto-Regular.ttf", 14, 0);
    // font_to_use = nk_font_atlas_add_from_file(atlas, "./kenvector_future_thin.ttf", 13, 0);
    font_to_use = nk_font_atlas_add_from_file(atlas, "./ProggyClean.ttf", 12, 0);
    // font_to_use = nk_font_atlas_add_from_file(atlas, "./ProggyTiny.ttf", 10, 0);
    // font_to_use = nk_font_atlas_add_from_file(atlas, "./Cousine-Regular.ttf", 13, 0);
    nk_orx_font_stash_end();

    /*nk_style_load_all_cursors(ctx, atlas->cursors);*/
    nk_style_set_font(ctx, &font_to_use->handle);

#ifdef INCLUDE_STYLE
//    set_style(ctx, THEME_WHITE);
//    set_style(ctx, THEME_RED);
//    set_style(ctx, THEME_BLUE);
//    set_style(ctx, THEME_DARK);
#endif

    /* add events to manage */
    orxEvent_AddHandler(orxEVENT_TYPE_RENDER, NuklearOrx_EventHandler);
    orxEvent_AddHandler(orxEVENT_TYPE_VIEWPORT, NuklearOrx_EventHandler);
    orxEvent_AddHandler(orxEVENT_TYPE_DISPLAY, NuklearOrx_EventHandler);

    // Creates main viewport
    gpstMainViewport = orxViewport_CreateFromConfig("MainViewport");

    // Gets main camera
    gpstMainCamera = orxViewport_GetCamera(gpstMainViewport);
            
    return orxSTATUS_SUCCESS;
    }

//////////////////////////////////////////////////////////////////////////
orxSTATUS orxFASTCALL NuklearOrx_Run()
    {
    nk_orx_new_frame();
    struct nk_context * ctx = (struct nk_context *)m_pNkContext;

#ifdef RUN_ORX_DEMO
    struct nk_color background = nk_rgb(28, 48, 62);

    orxFLOAT viewport_w, viewport_h;
    orxViewport_GetSize(gpstMainViewport, &viewport_w, &viewport_h);

    if (nk_begin(ctx, "Demo", nk_rect(50, 50, 230, 250),
        NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE |
        NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE))
        {
        enum { EASY, HARD };
        static int op = EASY;
        static int property = 20;
        nk_layout_row_static(ctx, 30, 80, 1);
        if (nk_button_label(ctx, "button"))
            fprintf(stdout, "button pressed\n");

        nk_layout_row_dynamic(ctx, 30, 2);
        if (nk_option_label(ctx, "easy", op == EASY)) op = EASY;
        if (nk_option_label(ctx, "hard", op == HARD)) op = HARD;

        nk_layout_row_dynamic(ctx, 25, 1);
        nk_property_int(ctx, "Compression:", 0, &property, 100, 10, 1);

        nk_layout_row_dynamic(ctx, 20, 1);
        nk_label(ctx, "background:", NK_TEXT_LEFT);
        nk_layout_row_dynamic(ctx, 25, 1);
        if (nk_combo_begin_color(ctx, background, nk_vec2(nk_widget_width(ctx), 400))) {
            nk_layout_row_dynamic(ctx, 120, 1);
//            background = nk_color_picker(ctx, background, NK_RGBA);
            nk_layout_row_dynamic(ctx, 25, 1);
            background.r = (nk_byte)nk_propertyi(ctx, "#R:", 0, background.r, 255, 1, 1);
            background.g = (nk_byte)nk_propertyi(ctx, "#G:", 0, background.g, 255, 1, 1);
            background.b = (nk_byte)nk_propertyi(ctx, "#B:", 0, background.b, 255, 1, 1);
            background.a = (nk_byte)nk_propertyi(ctx, "#A:", 0, background.a, 255, 1, 1);
            nk_combo_end(ctx);
            }
        }
    nk_end(ctx);
#endif

    /* -------------- EXAMPLES ---------------- */
#ifdef INCLUDE_CALCULATOR
    calculator(ctx);
#endif
#ifdef INCLUDE_OVERVIEW
    overview(ctx);
#endif
#ifdef INCLUDE_NODE_EDITOR
    node_editor(ctx);
#endif

    return orxSTATUS_SUCCESS;
    }

//////////////////////////////////////////////////////////////////////////
void  orxFASTCALL NuklearOrx_Exit()
    {
    nk_orx_shutdown();
    }


//#ifndef __orxMSVC__
//////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv)
    {
    /* Inits and executes orx */
    orx_Execute(argc, argv, NuklearOrx_Init, NuklearOrx_Run, NuklearOrx_Exit);
    return EXIT_SUCCESS;
    }
// #else  // __orxMSVC__
// int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
//     {
//     UNUSED(hInstance);
//     UNUSED(hPrevInstance);
//     UNUSED(lpCmdLine);
//     UNUSED(nCmdShow);
// 
//     // Inits and executes orx
//     orx_WinExecute(NuklearOrx_Init, NuklearOrx_Run, NuklearOrx_Exit);
// 
//     // Done!
//     return EXIT_SUCCESS;
//     }
// #endif // __orxMSVC__


