# NuklearOrx v0.1.0 -  Nuklear for Orx Engine

NuklearOrx is an integration of Nuklear for the Orx Game Engine.

***

## Building NuklearOrx

The library depends on:
- Orx Game Engine library
- Nuklear library

Before building NuklearOrx, you need to perform few steps.

### Build Orx Game Engine

Please, clone Orx Game Engine from here (https://github.com/orx/orx) and follow instructions on how to build it.  
If you're a Linux/OSx user, please take also a look here (http://orx-project.org/wiki/en/tutorials/main#linux). 

When you've done with cloning, setupping and building, please check if in your environment variables you have one named ORX that points to the Orx Game Engine folder.

### Setup NuklearOrx

After cloning NuklearOrx repository you need to run **./setup(.bat/.sh)** and let it create all project files you need.

NuklearOrx library depends on Nuklear (https://github.com/vurtun/nuklear) hence a submodule has been used.

Ensure the submodules required by this repo are updated before building. To do so:

* cd into the NuklearOrx repo folder
* git submodule init
* git submodule update


## Test application

Under **(NuklearOrx folder)/test** folder you can find a test application you can use to play with this integration.  
You can move to **(NuklearOrx folder)/build/(platform)** and select your favourite IDE folder and use project files to build NuklearOrx test application.  
You can find produced executable file in **(NuklearOrx folder)/bin/** folder.

For example, with gmake (mingw32):

```
mingw32-make imgui_orx config=debug
mingw32-make imgui_orx config=release
```

### IMPORTANT for Visual Studio users: 
When you load the solution you need to right-click on the solution and then "Retarget Solution".

***

## How to use NuklearOrx Library

Depending to the IDE you're using, you need to specify, in your project, where the compiler will find needed header files.
In addition to Orx Game Engine header folder, you need to specify two more folders:
- **(NuklearOrx folder)/include**
- **(NuklearOrx folder)/nuklear**

As you can see in **(NuklearOrx folder)/test/main.cpp**, you need to include:

- nuklear_orx.h
- orx.h

If you take a look at the example file, you can see in *NuklearOrx_Init()* there's Nuklear context initialization.
After the initialization, Orx will continuously call *NuklearOrx_Run()*, the place where we tell Nuklear what controls we want to draw (see Nuklear tutorials)
To render all of Nuklear controls, we need our application to react *orxEVENT_TYPE_RENDER* events with *orxRENDER_EVENT_STOP* id.  
That's needed because we want to render Nuklear stuff on top of other game stuff.

***


## License

This code is distributed under WTFPL License (http://www.wtfpl.net/)  
Enjoy and don't fuck about my Whitesmiths coding style!
